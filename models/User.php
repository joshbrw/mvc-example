<?php

class User {

	/**
	 * An array of users, acting as a form of database
	 * @var array
	 */
	public $users = array(
		[
			'name' => 'Josh Brown',
			'age' => 19,
			'occupation' => 'PHP Developer'
		],
	[
			'name' => 'Nathan Parsons',
			'age' => 19,
			'occupation' => 'Professional Idiot'
		],
	);

	/**
	 * Find a specific user. Only pass 0 or 1 as anything else will fail
	 *
	 * @param $id
	 * @return mixed
	 */
	public function find($id) {
		return (isset($this->users[$id])) ? $this->users[$id] : null;
	}
}