#MVC Example

This is a very basic example of how MVC works. The `index.php` file in the root directory is the 'bootstrap' file which loads in the `HomeController` and fires the `index()` method.