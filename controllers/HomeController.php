<?php

class HomeController {

	/**
	 * This is the method called for the homepage.
	 */
	public function index() {
		$user = new User;

		/**
		 * Run the 'find' method on the User model.
		 *
		 * If you change the 0 in this to a 1 it will return the second user in the array.
		 */
		$user = $user->find(0);

		/**
		 * Render out the view to the page.
		 */
		require "views/index.php";
	}

}