<?php
/**
 * This is the worlds most basic example of MVC.
 * This isn't how it works in the real world, but the Model/Controller/View are all
 * realistic examples, I guess.
 */

/**
 * Load the files we need
 */
require "controllers/HomeController.php";
require "models/User.php";

/**
 * Make new instances of the controller and model
 */
$controller = new HomeController;

/**
 * Call the `index` action on the Home Controller.
 * In frameworks this is handled using some kind of routing mechanism,
 * but we can just force it here.
 */
$controller->index();